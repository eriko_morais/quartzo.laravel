<?php namespace App;

use Illuminate\Database\Eloquent\Model;

class Inquilino extends Model {

	protected $fillabe = [
            'nome',
            'nacionalidade', 
            'estado_civil',
            'profissao',
            'rg',
            'cpf',
            'endereco',
            'bairro',
            'fone',
            'email',
            'obs'
        ];

}
