<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSalaTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
                Schema::create('sala', function(Blueprint $table){
                    
                    $table->increments('id');
                    $table->string('numero',6);
                    $table->integer('dimensao');
                    $table->string('finalidade',20);
                    $table->string('endereco',150);
                    $table->string('obs',200)->nullable();
                    
                });
	}

	/**
	 * Reverse the migrations.
	 
	 * @return void
	 */
	public function down()
	{
		Schema::drop('sala');
	}

}
