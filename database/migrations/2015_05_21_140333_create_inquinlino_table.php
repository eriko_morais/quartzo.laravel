<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateInquinlinoTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('inquilino', function(Blueprint $table)
		{
			$table->increments('id');
			$table->string('nome', 100);
                        $table->string('nacionalidade', 50);
                        $table->string('estado_civil', 50);
                        $table->string('profissao', 100);
                        $table->integer('rg');
                        $table->integer('cpf')->unique();
			$table->string('endereco',150);
			$table->string('bairro', 100);
                        $table->string('fone', 12);
			$table->string('email',100);
                        $table->string('obs',200)->nullable();
                        
                        
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('inquilino');
	}

}
