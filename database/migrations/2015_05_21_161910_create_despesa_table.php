<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateDespesaTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
                Schema::create('periodo',function(Blueprint $table){
                    
                    $table->increments('id');
                    $table->integer('mes')->unsigned();
                    $table->integer('ano')->unsigned();
                    
                });
                
                 Schema::create('modalidade',function(Blueprint $table){
                    
                    $table->increments('id');
                    $table->string('nome',50);
                   
                    
                });
            
		Schema::create('despesa', function(Blueprint $table){
                    
                    $table->increments('id');
                    $table->decimal('valor');
                    $table->timestamp('data');
                    $table->string('obs',200)->nullable();
                    $table->char('modalidade',1);
                    $table->string('descricao',45)->nullable();
                    
                    $table->integer('id_sala')->unsigned();
                    $table->foreign('id_sala')
                          ->references('id')->on('sala');
                    
                    $table->integer('id_categoria')->unsigned();
                    $table->foreign('id_categoria')
                          ->references('id')->on('categoria_despesa');
                    
                    $table->integer('id_titulo')->unsigned();
                    $table->foreign('id_titulo')
                          ->references('id')->on('titulo_despesa');
                    
                    $table->integer('id_periodo')->unsigned();
                    $table->foreign('id_periodo')
                          ->references('id')->on('periodo');
                    
                    $table->integer('id_modalidade')->unsigned();
                    $table->foreign('id_modalidade')
                          ->references('id')->on('modalidade');
                });
                
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		 Schema::drop('despesa');
                 Schema::drop('modalidade');
                 Schema::drop('periodo');
	}

}
