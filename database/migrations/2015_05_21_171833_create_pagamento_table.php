<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePagamentoTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		 Schema::create('pagamento', function(Blueprint $table){
                    
                    $table->increments('id');
                    $table->decimal('valor',10,0);
                    $table->timestamp('data');
                    $table->decimal('acrescimo',10,0)->nullable();
                    
                    $table->integer('id_periodo')->unsigned();
                    $table->foreign('id_periodo')
                          ->references('id')->on('periodo');
                    
                    $table->integer('id_inquilino')->unsigned();
                    $table->foreign('id_inquilino')
                          ->references('id')->on('inquilino');
                    
                    $table->integer('id_contrato')->unsigned();
                    $table->foreign('id_contrato')
                          ->references('id')->on('contrato');
                });
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		 Schema::drop('pagamento');
	}

}
