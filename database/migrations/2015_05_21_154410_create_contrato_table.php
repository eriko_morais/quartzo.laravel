<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateContratoTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		 Schema::create('contrato', function(Blueprint $table){
                    
                    $table->increments('id');
                    $table->date('data_inicio');
                    $table->date('data_termino')->nullable();
                    $table->integer('dia_vencimento');
                    $table->boolean('ativo');
                    $table->integer('id_sala')->unsigned();
                    
                    $table->foreign('id_sala')
                          ->references('id')->on('sala');
                });
                
                Schema::create('contrato_inquilino',function(Blueprint $table){
                    
                    $table->integer('id_contrato')->unsigned()->index();
                    $table->foreign('id_contrato')->references('id')->on('contrato');
                    
                    $table->integer('id_inquilino')->unsigned()->index();
                    $table->foreign('id_inquilino')->references('id')->on('inquilino');
                    
                });
                
                
                
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('contrato_inquilino');
                Schema::drop('contrato');
	}

}
