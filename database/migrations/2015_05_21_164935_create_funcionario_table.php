<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateFuncionarioTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		  Schema::create('funcionario', function(Blueprint $table){
                    
                    $table->increments('id');
                    $table->string('nome',150);
                    $table->string('enderco',150);
                    $table->string('estado_civil',8);
                    $table->integer('n_filhos_menores');
                    $table->integer('rg');
                    $table->integer('cpf');
                    $table->integer('ctps');
                    $table->integer('pis_pasep');
                    $table->string('funcao',200);
                    $table->date('data_admissao');
                    $table->date('data_demissao')->nullable();
                    $table->decimal('salario', 10, 0);
                    $table->integer('carga_horaria');
                    $table->string('obs',200)->nullable();
                    $table->string('email',150);
                    
                });
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('funcionario');
	}

}
